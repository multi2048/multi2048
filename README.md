# Multi2048
Implementing the game 2048 in a buttload of different languages and variants,
in an effort to get slightly familiar with the languages and to see differences
in implementing the same thing.

Starting off with Python, then C, Powershell, and then it starts to get chaotic.

Each of them have their own repository, this is mostly a way to make it easy
fetching it all.

## Confirmed working (at the given commit for each repo)
* js2048
  * 14debe7 - Node, Spidermonkey, and HTML5
* py2048
  * python3
    * c205847 - QML, Curses, TTY
* bas2048
  * 85eecd6 - QBasic, TI-86
* go2048
  * 34dc927
* lua2048
  * 53ba262
* pwsh2048
  * bb14b9c
* c2048
  * 2020446 - TTY and screen, library or single binary.
* go2048
  * 0643db7
* pl2048
  * 90a5454
